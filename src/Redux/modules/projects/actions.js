const addProjects = (projects) => ({
  type: "@add_projects",
  projects,
});

export default addProjects;
