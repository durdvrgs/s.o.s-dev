import StyledContainer from "./style";
import Image from "../../Atoms/Image";
import Button from "../../Atoms/Button";
import { Link } from 'react-scroll'

import { useMediaQuery, Grid } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  container: { flexGrow: 1 },
}));

const Slogan = ({ slogan, img, img2 }) => {
  const classes = useStyles();
  const isMobile = useMediaQuery("(max-width:767px)");
  const isMediumMobile = useMediaQuery("(min-width:540px)");

  return (
    <>
      {isMobile ? (
        <Grid
          container
          className={classes.container}
          justify="center"
          alignItems="center"
          style={{ marginBottom: "15px" }}
        >
          <Grid item xs={12} align="center" style={{ marginTop: "10px" }}>
            <Image height="auto" width={isMediumMobile ? '70vw' : "90vw"} src={slogan} />
          </Grid>

          <Grid item xs={12} align="center" style={{ marginTop: "10px" }}>
            <Image src={img} width="210px" />
          </Grid>
          <Grid item xs={12} align="center" style={{ marginTop: "15px" }}>
            <Link to="box" spy={true} smooth={true}>
              <Button text="Saiba Mais" classe="buttonSaibaMais" />
             </Link>
          </Grid>

          <Grid item xs={12} align="center" style={{ marginTop: "20px" }}>
            <Image width="210px" src={img2} />
          </Grid>
        </Grid>
      ) : (
        <StyledContainer>
          <div className="sloganbutton">
            <Image src={slogan} />
            <Link to="box" spy={true} smooth={true}>
              <Button text="Saiba Mais" classe="buttonSaibaMais" classe = 'landingButton'/>
             </Link>
          </div>
          <div className="pessoas">
            <Image src={img} />
          </div>
        </StyledContainer>
      )}
    </>
  );
};

export default Slogan;
