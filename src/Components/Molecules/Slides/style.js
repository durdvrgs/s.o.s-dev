import { makeStyles } from "@material-ui/core/styles";
import styled from "styled-components";

export const useStyles = makeStyles({
  root: {
    width: "100%",
    backgroudColor: "#EFDCBD",
  },
  cardContent: {
    overflowY: 'auto',

  }
});

export const SlideContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 50vw;
  height: 35vh;
  margin: 0 auto;

  @media (min-width: 280px){
    width: 90vw;
  }

  .MuiCardContent-root {
    height: 170px;
    border: 2px solid #efdcbd;
    background-color: #17223a;

    @media (min-width: 2560px){
      height: 20vh;
    }
  }
`;
