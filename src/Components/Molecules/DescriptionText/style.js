import styled from "styled-components";

const Section2Content = styled.section`
  width: 100vw;
  display: flex;
  align-items: center;
  height: 50vh;

  .description-text-content {
    width: 50vw;
    display: flex;
    flex-direction: column;
    margin: 0px;
    color: #efdcbd;
    padding-left: 2vw;
    padding-right: 2vw;
  }

  .description {
    width: 100%;
  }

  .second-image-content {
    width: 50vw;
    display: flex;
    justify-content: center;
    align-items: center;

    
    img {
        width: 35vw;
        height: auto;
        @media (min-width: 1440px){
        width: 30vw;
      }
  }
}
`;

export default Section2Content;
