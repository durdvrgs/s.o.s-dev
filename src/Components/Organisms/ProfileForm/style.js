import styled from "styled-components";

export const FormContainer = styled.div`
  height: 80vh;
  width: 60%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  border: 1px solid #efdcbd;

  @media (min-width: 280px) and (max-width: 320px) {
    height: 87vh;
  }
  @media (min-width: 320px) and (max-width: 768px) {
    height: 86vh;
  }
`;

export const StyledForm = styled.form`
  width: 100%;
  height: 80%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  justify-content: center;

  @media (min-width: 280px) and (max-width: 320px) {
    width: 95%;
    height: 100%;
  }
  @media (min-width: 320px) and (max-width: 360px) {
    width: 95%;
    height: 100%;
  }
  @media (min-width: 375px) and (max-width: 425px) {
    width: 95%;
    height: 100%;
  }
  @media (min-width: 425px) and (max-width: 540px) {
    width: 95%;
    height: 100%;
  }
  @media (min-width: 768px) and (max-width: 1024px) {
    width: 75%;
    height: 100%;
  }
`;

export const TechContainer = styled.div`
  width: 100%;
  height: 10%;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const TechBox = styled.div`
  height: 30%;
  display: flex;
  justify-content: center;
  align-items: center;

  button {
    width: 5%;
    height: 100%;
  }
`;

export const AvatarBox = styled.div`
  height: 20%;
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

export const Image = styled.img`
  width: 100px;
  height: 100px;
`;
