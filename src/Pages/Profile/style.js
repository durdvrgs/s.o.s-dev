import styled from "styled-components";

export const Container = styled.div`
  height: 93vh;
  width: 33.5vw;
  background-color: #17223a;
  border-right: 1px solid #efdcbd;
  align-self: left;
  margin-top: -1vh;
  font-size: 1.5vw;
  word-wrap: break-word;
  position: fixed;

  display: flex;
  flex-direction: column;
  align-items: center;

  @media (min-width: 280px) and (max-width: 320px) {
    font-size: 3.3vw;
    height: 90.5vh;
    margin-top: -2vh;
  }

  @media (min-width: 320px) and (max-width: 360px) {
    font-size: 3vw;
    height: 89.5vh;
    margin-top: -3vh;
  }

  @media (min-width: 360px) and (max-width: 375px) {
    font-size: 3vw;
    height: 91vh;
    margin-top: -3vh;
  }

  @media (min-width: 375px) and (max-width: 425px) {
    font-size: 3vw;
    height: 93.8vh;
    margin-top: -3vh;
  }

  @media (min-width: 425px) and (max-width: 540px) {
    font-size: 3vw;
    height: 95vh;
    margin-top: -3vh;
  }

  @media (min-width: 540px) and (max-width: 767px) {
    font-size: 2.5vw;
    height: 93vh;
    margin-top: -2vh;
  }

  @media (max-width: 767px) {
    width: 100vw;
    border: none;
  }

  @media (min-width: 1440px) {
    font-size: 1vw;
  }

  @media (min-width: 2560px) {
    height: 95vh;
  }

  img {
    width: 10vw;
    height: auto;

    @media (min-width: 280px) and (max-width: 320px) {
      width: 30vw;
    }

    @media (min-width: 320px) and (max-width: 540px) {
      width: 25vw;
    }

    @media (min-width: 540px) and (max-width: 767px) {
      width: 18vw;
    }

    @media (min-width: 1440px) {
      width: 7vw;
    }

    @media (min-width: 2560px) {
      width: 6vw;
    }
  }
`;

export const TechContainer = styled.div`
  width: 33.5vw;
  height: 17vh;
  display: flex;
  flex-direction: row;
  margin-bottom: 2vh;

  @media (max-width: 767px) {
    width: 100vw;
  }

  .techs,
  .techsLevel {
    padding: 0 1vw 0 1vw;
    display: flex;
    flex-direction: column;
    width: 16.75vw;
    height: 17vh;
    color: #efdcbd;
    @media (max-width: 767px) {
      width: 50vw;
    }
  }
`;

export const PendingProjectsContainer = styled.div`
  width: 33.5vw;
  height: 17vh;
  padding: 0 1vw 0 1vw;
  color: #efdcbd;
  margin-bottom: 2vh;
  @media (max-width: 767px) {
    width: 100vw;
    padding: 0 5vw 0 5vw;
  }
`;

export const CompletedProjectsContainer = styled.div`
  width: 33.5vw;
  height: 17vh;
  display: flex;
  flex-direction: row;
  margin-bottom: 2vh;

  .project,
  .deploy {
    padding: 0 1vw 0 1vw;
    display: flex;
    flex-direction: column;
    width: 16.75vw;
    height: 17vh;
    color: #efdcbd;
    @media (max-width: 767px) {
      width: 50vw;
      padding: 0 5vw 0 5vw;
    }
  }

  @media (max-width: 767px) {
    width: 100vw;
  }
`;

export const ContactContainer = styled.div`
  width: 33.5vw;
  height: 17vh;
  display: flex;
  flex-direction: column;
  margin-bottom: 2vh;

  @media (max-width: 767px) {
    width: 100vw;
  }

  .contactsContainer {
    width: 33.5vw;
    display: flex;

    .webContacts {
      border-right: 1px solid #efdcbd;
    }

    .webContacts,
    .socialContacts {
      padding: 0 1vw 0 1vw;
      display: flex;
      flex-direction: column;
      align-items: center;
      width: 16.75vw;
      height: 12vh;
      color: #efdcbd;

      div {
        height: 3vh;
        word-break: break-all;
      }
      @media (min-width: 280px) and (max-width: 320px) {
        width: 50vw;
        height: 10vh;
        div {
          height: 2.5vh;
        }
      }
      @media (min-width: 320px) and (max-width: 540px) {
        width: 50vw;
        height: 9vh;
        div {
          height: 2.25vh;
        }
      }
      @media (min-width: 540px) and (max-width: 768px) {
        width: 50vw;
        height: 10vh;
        div {
          height: 2.5vh;
        }
      }
    }
    @media (max-width: 767px) {
      width: 100vw;
    }
  }
`;

export const ButtonContainer = styled.div`
  width: 33.5vw;
  display: flex;
  justify-content: space-around;

  @media (max-width: 767px) {
    width: 100vw;
  }
`;

export const SocialMedia = styled.div`
  display: flex;
  align-items: center;

  a {
    text-decoration: none;
    color: #efdcbd;
    font-weight: bold;
    :hover {
      color: #eb5c82;
    }
  }
`;
