import styled from "styled-components";

export const RenderBox = styled.div`
  min-height: 92.3vh;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  @media (min-width: 280px) and (max-width: 320px) {
    height: 88.5vh;
    justify-content: flex-start;
  }

  @media (min-width: 320px) and (max-width: 767px) {
    height: 90.5vh;
    justify-content: flex-start;
  }
`;
